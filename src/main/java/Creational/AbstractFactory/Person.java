package Creational.AbstractFactory;

import Creational.AbstractFactory.clothingItems.boots.Boots;
import Creational.AbstractFactory.clothingItems.hat.Hat;
import Creational.AbstractFactory.clothingItems.pants.Pants;

public class Person {
    private Boots boots;
    private Pants pants;
    private Hat hat;

    public Person(Boots boots, Pants pants, Hat hat) {
        this.boots = boots;
        this.pants = pants;
        this.hat = hat;
    }

    public final String getClothesInfo(){
        return String.format("На ногах надеты %s, в качетве штанов надеты %s, а на голове - %s.", boots.getName(), pants.getName(), hat.getName());
    }
}
