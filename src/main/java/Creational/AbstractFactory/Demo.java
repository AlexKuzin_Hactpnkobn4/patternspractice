package Creational.AbstractFactory;

import Creational.AbstractFactory.factories.ElegantPersonFactory;
import Creational.AbstractFactory.factories.PersonFactory;
import Creational.AbstractFactory.factories.SportPersonFactory;

import java.util.Scanner;

public class Demo {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("""
                               Введите номер фабрики:
                               1-Фабрика спортивного человека
                               2-Фабрика элегантного человека
                               """);
        int i =  scanner.nextInt();

        if(i<1 || i>2){
            System.out.println("Введено неверное число. Попробуйте еще раз...");
            i =  scanner.nextInt();
        }
        PersonFactory factory = i == 1 ? new SportPersonFactory() : new ElegantPersonFactory();

        System.out.println(factory.createPerson().getClothesInfo());


    }
}
