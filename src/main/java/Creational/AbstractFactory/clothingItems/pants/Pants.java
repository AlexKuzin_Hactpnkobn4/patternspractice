package Creational.AbstractFactory.clothingItems.pants;

public interface Pants {
    public String getName();
}
