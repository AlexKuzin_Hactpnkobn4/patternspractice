package Creational.AbstractFactory.clothingItems.pants;

public class Trousers implements Pants {
    @Override
    public String getName() {
        return "Брюки";
    }
}
