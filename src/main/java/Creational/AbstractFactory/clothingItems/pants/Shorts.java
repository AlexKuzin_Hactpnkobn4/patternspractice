package Creational.AbstractFactory.clothingItems.pants;

public class Shorts implements Pants {
    @Override
    public String getName() {
        return "Шорты";
    }
}
