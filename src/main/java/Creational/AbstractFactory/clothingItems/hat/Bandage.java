package Creational.AbstractFactory.clothingItems.hat;

public class Bandage implements Hat {
    @Override
    public String getName() {
        return "Повязка";
    }
}
