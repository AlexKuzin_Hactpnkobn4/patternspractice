package Creational.AbstractFactory.clothingItems.hat;

public class Cap implements Hat {
    @Override
    public String getName() {
        return "Кепка";
    }
}
