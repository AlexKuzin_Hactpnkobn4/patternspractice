package Creational.AbstractFactory.clothingItems.hat;

public interface Hat {
    public String getName();
}
