package Creational.AbstractFactory.clothingItems.boots;

public class LevisBoots implements Boots {
    @Override
    public String getName() {
        return "Ботинки Levis";
    }
}
