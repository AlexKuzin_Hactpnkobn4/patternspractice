package Creational.AbstractFactory.clothingItems.boots;

public interface Boots {
    public String getName();
}
