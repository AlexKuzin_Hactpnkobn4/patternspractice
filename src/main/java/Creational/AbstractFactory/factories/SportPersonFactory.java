package Creational.AbstractFactory.factories;

import Creational.AbstractFactory.Person;
import Creational.AbstractFactory.clothingItems.boots.NikeBoots;
import Creational.AbstractFactory.clothingItems.hat.Cap;
import Creational.AbstractFactory.clothingItems.pants.Shorts;

public class SportPersonFactory implements  PersonFactory{
    @Override
    public Person createPerson() {
        return new Person(new NikeBoots(), new Shorts(), new Cap());
    }
}
