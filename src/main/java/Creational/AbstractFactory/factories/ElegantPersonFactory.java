package Creational.AbstractFactory.factories;

import Creational.AbstractFactory.Person;
import Creational.AbstractFactory.clothingItems.boots.LevisBoots;
import Creational.AbstractFactory.clothingItems.hat.Bandage;
import Creational.AbstractFactory.clothingItems.pants.Trousers;

public class ElegantPersonFactory implements PersonFactory {
    @Override
    public Person createPerson() {
        return new Person(new LevisBoots(), new Trousers(), new Bandage());
    }
}
