package Creational.AbstractFactory.factories;

import Creational.AbstractFactory.Person;

public interface PersonFactory {
    public Person createPerson();
}
