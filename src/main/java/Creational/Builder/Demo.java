package Creational.Builder;

public class Demo {
    public static void main(String[] args) {
        Home home = Home.builder().x(1).y(2).x(3).build();
        System.out.println(home);
    }
}
