package Creational.Builder;

public class Home implements Cloneable{

    private int x;
    private int y;

    public int getX() {return x;}
    public int getY() {return y;}

    private Home(){}

    protected Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Home{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static HomeBuilder builder(){
        return new HomeBuilder();
    }

    public static class HomeBuilder {
        private Home home = new Home();

        private HomeBuilder() {}

        public HomeBuilder x(int x){
            home.x = x;
            return this;
        }

        public HomeBuilder y(int y){
            home.y = y;
            return this;
        }

        public Home build(){
            return (Home) home.clone();
        }
    }
}
