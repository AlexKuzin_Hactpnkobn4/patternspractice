package Creational.FactoryMethod;

import Creational.FactoryMethod.shops.ClothingShop;
import Creational.FactoryMethod.shops.FoodShop;
import Creational.FactoryMethod.shops.Shop;

import java.util.Arrays;
import java.util.List;

public class Demo {

    public static void main(String[] args) {
        List<Shop> shops = Arrays.asList(new ClothingShop(), new FoodShop());

        System.out.println("****************************************");
        for (Shop shop : shops){
            System.out.println("Класс текущего магазина - " + shop.getClass().getName());
            System.out.println("Вывеска магазина: " + shop.getSignboard().getTitle());
            System.out.println("****************************************");
        }
    }
}
