package Creational.FactoryMethod.shops;

import Creational.FactoryMethod.signboard.Signboard;

public interface Shop {
    public Signboard getSignboard();
}
