package Creational.FactoryMethod.shops;

import Creational.FactoryMethod.signboard.MagnitSignboard;
import Creational.FactoryMethod.signboard.Signboard;

public class FoodShop implements Shop {
    @Override
    public Signboard getSignboard() {
        return new MagnitSignboard();
    }
}
