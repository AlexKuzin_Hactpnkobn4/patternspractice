package Creational.FactoryMethod.shops;

import Creational.FactoryMethod.signboard.BershkaSignboard;
import Creational.FactoryMethod.signboard.Signboard;

public class ClothingShop implements Shop {
    @Override
    public Signboard getSignboard() {
        return new BershkaSignboard();
    }
}
