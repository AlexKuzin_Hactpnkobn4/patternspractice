package Creational.FactoryMethod.signboard;

public class BershkaSignboard implements Signboard{
    @Override
    public String getTitle() {
        return "Bershka";
    }
}
