package Creational.FactoryMethod.signboard;

public class MagnitSignboard implements Signboard {
    @Override
    public String getTitle() {
        return "Магнит";
    }
}
