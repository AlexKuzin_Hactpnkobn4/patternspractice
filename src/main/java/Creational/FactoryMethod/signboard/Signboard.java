package Creational.FactoryMethod.signboard;

public interface Signboard {
    public String getTitle();
}
