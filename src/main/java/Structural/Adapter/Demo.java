package Structural.Adapter;

public class Demo {

    public static void main(String[] args) {
        RoundHole roundHole = new RoundHole(10);
        RoundPeg roundPeg = new RoundPeg(9);

        System.out.println(String.format("Round peg with radius %s is fits in hole with radius %s: %s ",
                                            roundPeg.getRadius(),
                                            roundHole.getRadius(),
                                            roundHole.fits(roundPeg)));

        SquarePeg squarePeg = new SquarePeg(14);
//        roundHole.fits(squarePeg); // Doesn't work
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(squarePeg);

        System.out.println(String.format("Square peg with width %s (radius is %s) is fits in hole with radius %s: %s ",
                                            squarePegAdapter.getWidth(),
                                            squarePegAdapter.getRadius(),
                                            roundHole.getRadius(),
                                            roundHole.fits(squarePegAdapter)));

    }

}